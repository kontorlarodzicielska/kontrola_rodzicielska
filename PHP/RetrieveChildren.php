<?php

/*
 * Following code will list all the products
 */

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/db_connect.php';

// connecting to db
$db = new DB_CONNECT();



if (isset($_GET["uid"]) ) {
    $uid_parent = $_GET['uid'];
	// get all products from products table
	$result = mysql_query("SELECT * FROM relations WHERE uid_p = '$uid_parent'") or die(mysql_error());

	// check for empty result
	if (mysql_num_rows($result) > 0) {
		// looping through all results
		// products node
		$response["child"] = array();

		while ($row = mysql_fetch_array($result)) {
			// temp user array
			$dane = array();

			$dane["uid_child"] = $row["uid_c"];
			$dane["name"] = $row["name"];
			$dane["phone"] = $row["phone"];
		  
			// push single product into final response array
			array_push($response["child"], $dane);
		}

		$response["success"] = 1;
		$response["message"] = "All child retrieve!";
		// echoing JSON response
		echo json_encode($response);
	} else {
		// no products found
		$response["success"] = 0;
		$response["message"] = "You have not assigned children!";

		// echo no users JSON
		echo json_encode($response);
		}
}
else{
	// no products found
	$response["success"] = 0;
	$response["message"] = "Missing data";

	// echo no users JSON
	echo json_encode($response);
}
?>
