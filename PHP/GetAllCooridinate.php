<?php

/*
 * Following code will list all the products
 */

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/db_connect.php';

// connecting to db
$db = new DB_CONNECT();

if (isset($_GET["uid_child"]) ) {
    $uid_child= $_GET['uid_child'];
	
	// get all products from products table
	$result = mysql_query("SELECT * FROM coordinates WHERE (uid='$uid_child')") or die(mysql_error());

	// check for empty result
	if (mysql_num_rows($result) > 0) {
		// looping through all results
		// products node
		$response["coord"] = array();
		
		while ($row = mysql_fetch_array($result)) {
			// temp user array
			$data = array();
			$data["lat"] = $row["lat"];
			$data["lon"] = $row["lon"];
			$data["date"] = $row["date"];
			$data["time"] = $row["time"];

			// push single product into final response array
			array_push($response["coord"], $data);
		}
		
		$response["success"] = 1;
		$response["message"] = "Cooridinate collected!";
		// echoing JSON response
		echo json_encode($response);
	} else {
		// no products found
		$response["success"] = 0;
		$response["message"] = "No data to collect!";

		// echo no users JSON
		echo json_encode($response);
	}
}
else{
	// no products found
    $response["success"] = 0;
    $response["message"] = "Missing data";

    // echo no users JSON
    echo json_encode($response);
}
?>
