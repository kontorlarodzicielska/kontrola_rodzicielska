<?php

/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */

// array for JSON response
$response = array();

// check for required fields
if (isset($_POST['name']) && isset($_POST['phone']) && isset($_POST['uid_child'])) {
	
	$name = $_POST['name'];
	$phone = $_POST['phone'];
	$uid_child = $_POST['uid_child'];

    // include db connect class
    require_once __DIR__ . '/db_connect.php';

    // connecting to db
    $db = new DB_CONNECT();

    // mysql inserting a new row
    $result = mysql_query("INSERT INTO child(name,phone,uid_child) VALUES('$name','$phone','$uid_child')");
    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Add child to database.";

        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 403;
        $response["message"] = "No connection to database.";

        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Missing data.";

    // echoing JSON response
    echo json_encode($response);
}
?>
