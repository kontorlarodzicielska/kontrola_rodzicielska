package pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.R;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.adapters.ViewPageAdapter;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.fragment.ChildrenManagerFragment;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.fragment.AddChildFragment;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.fragment.SettignsFragment;

/**
 * Created by Użytkownik on 27.11.2016.
 */
public class UserActivity extends AppCompatActivity {

    private final static String PREFERENCE_NAME = "ChildToShow";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.goToMapButton)
    FloatingActionButton goToMapButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_fragment);
        ButterKnife.bind(this);
//          Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
//        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

        setSupportActionBar(toolbar);
        setupViewPager(viewPager);
        tabs.setupWithViewPager(viewPager);
        setupListeners();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPageAdapter adapter = new ViewPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new SettignsFragment(), "Settings");
        adapter.addFragment(new ChildrenManagerFragment(), "Child Manager");
        adapter.addFragment(new AddChildFragment(), "Add Child");
        viewPager.setAdapter(adapter);
    }
    private void setupListeners(){
        goToMapButton.setImageResource(R.drawable.fab_button_999);
        goToMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> childToShow = new ArrayList<>();
                Set<String> tempSet = restoreChildListToShow();
                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                if(tempSet!=null) {
                    childToShow.addAll(tempSet);
                     intent.putStringArrayListExtra("childToShow", childToShow);
                    startActivity(intent);
                }
            }
        });
    }
    private Set<String> restoreChildListToShow(){
        SharedPreferences pref = getApplication().getSharedPreferences(PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return pref.getStringSet("childToShow",null);
    }
}
