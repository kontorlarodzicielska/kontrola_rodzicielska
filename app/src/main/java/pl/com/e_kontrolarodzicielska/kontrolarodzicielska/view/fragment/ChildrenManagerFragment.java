package pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.R;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.adapters.ChildItem;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.adapters.ChildListViewAdapter;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.utils.DatabaseMethods;

/**
 * Created by Użytkownik on 29.11.2016.
 */
public class ChildrenManagerFragment extends BaseFragment {

    private final static String GET_ALL_CHILD_ADDRESS = "RetrieveChildren.php";
    private final static String PREFERENCE_NAME = "ChildToShow";
    ListView childLV;
    View view;
    ChildListViewAdapter childListViewAdapter = null;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<ChildItem> childListArray;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.child_manager_fragment,container,false);
        initView();
        setListeners();
        return view;
    }

    private void initView(){
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        childLV = (ListView) view.findViewById(R.id.childList);
        childLV.setEmptyView(view.findViewById(R.id.empty_list_view));
     }
    private void setListeners(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                new GetChilds().execute(uid);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        childLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(childToShow.add(childListArray.get(i).getUid())){
                    view.setBackgroundColor(Color.BLUE);

                } else {
                    childToShow.remove(childListArray.get(i));
                    view.setBackgroundColor(Color.WHITE);
                }
                saveChildToShow();
            }
        });
    }

    public void saveChildToShow(){
        SharedPreferences sharedPref = getActivity().getSharedPreferences(PREFERENCE_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putStringSet("childToShow", childToShow);
        editor.apply();
    }


    class GetChilds extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            converter= new DatabaseMethods();
            jsonResponse= new JSONObject();
            messageResponse="";
            successResponse=0;
            childListArray= new ArrayList<>();
        }

        protected String doInBackground(String... args) {
            String uid = args[0];
            String params = "uid="+uid;
            jsonResponse = converter.GetMethodResult(params, GET_ALL_CHILD_ADDRESS);
            try {
                successResponse = jsonResponse.getInt("success");
                if(successResponse == 1) {
                    JSONArray childs = jsonResponse.getJSONArray("child");
                    for(int i =0 ; i<childs.length();i++){
                        JSONObject x = childs.getJSONObject(i);
                        childListArray.add(new ChildItem(1,x.getString("name"),x.getString("phone"),x.getString("uid_child")));
                       }
                }
                    messageResponse= jsonResponse.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            if(successResponse==1) {
                childListViewAdapter = new ChildListViewAdapter(getContext(), childListArray);
                childLV.setAdapter(childListViewAdapter);
                Toast.makeText(getContext(), messageResponse, Toast.LENGTH_SHORT).show();
                // updating listview
            }
            Toast.makeText(getActivity(),messageResponse, Toast.LENGTH_SHORT).show();
        }

    }
}
