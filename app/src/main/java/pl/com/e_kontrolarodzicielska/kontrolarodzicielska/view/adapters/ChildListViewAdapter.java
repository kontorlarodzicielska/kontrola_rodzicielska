package pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.adapters;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.R;

/**
 * Created by Użytkownik on 18.12.2016.
 */
public class ChildListViewAdapter extends BaseAdapter {

    Context context;
    //String[] nameList;
    //String[] telList;
    //ImageView[] imageList;
    ArrayList<ChildItem> childList = new ArrayList<>();

    private static LayoutInflater inflater=null;

    public ChildListViewAdapter(Context context, ArrayList<ChildItem> childList){
        this.context = context;
       // this.nameList = name;
        //this.telList = tel;
        //this.imageList = image;
        this.childList = childList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return childList.size();
    }

    @Override
    public Object getItem(int i) {
        return childList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi=view;
        if(view==null)
            vi = inflater.inflate(R.layout.child_item_lv, null);

        TextView name = (TextView)vi.findViewById(R.id.tvChildName); // title
        TextView telephone = (TextView)vi.findViewById(R.id.tvChildPhoneNumber); // artist name
        ImageView image=(ImageView)vi.findViewById(R.id.addImageB); // thumb image
        TextView uid = (TextView) vi.findViewById(R.id.tvChildUID);

        ChildItem item = childList.get(i);
        // Setting all values in listview
        name.setText(item.getNameChild());
        telephone.setText(item.getTelephoneNumber());
        image.setImageResource(R.drawable.add_image_button);
        uid.setText(item.getUid());
        return vi;
    }
}
