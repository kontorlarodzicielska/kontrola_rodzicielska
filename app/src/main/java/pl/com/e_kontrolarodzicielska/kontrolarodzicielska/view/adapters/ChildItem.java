package pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.adapters;

import java.io.Serializable;

/**
 * Created by Użytkownik on 18.12.2016.
 */
public class ChildItem implements Serializable {

    int image;
    String nameChild;
    String telephoneNumber;
    String uid;

    public ChildItem(int i,String n, String t,String u) {
        super();
        this.image = i;
        this.nameChild = n;
        this.telephoneNumber = t;
        this.uid = u;
    }
    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getNameChild() {
        return nameChild;
    }

    public void setNameChild(String nameChild) {
        this.nameChild = nameChild;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
