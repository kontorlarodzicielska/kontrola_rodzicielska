package pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.drive.realtime.internal.event.ObjectChangedDetails;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.R;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.model.Location;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.utils.DatabaseMethods;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private final static String PREFERENCE_NAME = "ChildToShow";
    private final static String GET_ALL_COORDINATE = "GetAllCooridinate.php";
    private GoogleMap mMap;
    private Set<String> childToShow;
    private Map<String,ArrayList<Location>> childMapLocation;
    private String tempChildUid;

    protected String messageResponse;
    protected JSONObject jsonResponse;
    protected DatabaseMethods converter;
    protected int successResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initVariable();
        getDataFormPreferences();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //// TODO: 19.04.2017 fit map to markers

        for (String uid : childToShow) {
            new GetAllCoord().execute(uid);
        }
    }
    private void initVariable() {
        childToShow = new HashSet<>();
        childMapLocation = new HashMap<>();
    }

    private void getDataFormPreferences(){
        SharedPreferences pref = getApplication().getSharedPreferences(PREFERENCE_NAME, Activity.MODE_PRIVATE);
        childToShow= pref.getStringSet("childToShow",null);
    }

    private void addMarkerOnMap(Double lat, Double lon,String date, String time){
        LatLng location = new LatLng(lat,lon);
        mMap.addMarker(new MarkerOptions()
                .position(location)
                .title(date)
                .snippet(time)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker2)));

    }

    class GetAllCoord extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            messageResponse = "";
            jsonResponse = new JSONObject();
            converter = new DatabaseMethods();
            successResponse=0;
            tempChildUid = "";
        }
        protected String doInBackground(String... args) {

            tempChildUid = args[0];
            String params = "uid_child="+tempChildUid;
            jsonResponse = converter.GetMethodResult(params,GET_ALL_COORDINATE);
            try {
                successResponse = jsonResponse.getInt("success");

                if(successResponse==1){
                    JSONArray coordinate = jsonResponse.getJSONArray("coord");
                    ArrayList<Location> coordList = new ArrayList<>();

                    for(int i =0 ; i <coordinate.length() ; i++){
                        JSONObject singleCoord = coordinate.getJSONObject(i);
                        Double lat = singleCoord.getDouble("lat");
                        Double lon = singleCoord.getDouble("lon");
                        String date = singleCoord.getString("date");
                        String time = singleCoord.getString("time");
                        coordList.add(new Location(lat,lon,date,time));
                    }
                    childMapLocation.put(tempChildUid,coordList);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String file_url) {
            runOnUiThread(new Runnable() {
                public void run() {

                    if (childMapLocation.containsKey(tempChildUid)) {
                        drawMarkersOnMap(childMapLocation.get(tempChildUid));
                        Toast.makeText(getApplicationContext(), messageResponse, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        private void drawMarkersOnMap(ArrayList<Location> coords){
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//            PolylineOptions rectOptions = new PolylineOptions();
//            rectOptions.width(2);

            for(int i = 0 ; i < coords.size() ; i++) {
                LatLng location = new LatLng(coords.get(i).getLat(),coords.get(i).getLon());
                addMarkerOnMap(coords.get(i).getLat(),coords.get(i).getLon(),coords.get(i).getDate(),coords.get(i).getTime());
//                rectOptions.add(location);
                builder.include(location);
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(),100));
        }
    }


}
