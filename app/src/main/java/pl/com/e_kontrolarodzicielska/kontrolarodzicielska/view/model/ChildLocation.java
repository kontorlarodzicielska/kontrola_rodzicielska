package pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.model;

import java.util.ArrayList;

/**
 * Created by Użytkownik on 18.04.2017.
 */
public class ChildLocation {

    public String childUID;
    public ArrayList<Location> childLocationArray;

    public ChildLocation(String childUID) {
        this.childUID = childUID;
        this.childLocationArray = new ArrayList<>();
    }
}
