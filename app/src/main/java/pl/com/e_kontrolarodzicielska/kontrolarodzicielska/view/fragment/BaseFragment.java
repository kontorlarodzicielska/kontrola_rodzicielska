package pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.utils.DatabaseMethods;

/**
 * Created by Użytkownik on 25.10.2016.
 */
public class BaseFragment extends Fragment {
    protected String lastPhoneNumber;
    protected String uid;
    protected String login;
    protected Integer rangeTime;
    protected Set<String> childToShow;

    protected String messageResponse;
    protected JSONObject jsonResponse;
    protected DatabaseMethods converter;
    protected int successResponse;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        rangeTime = 6;
        takeBundleFromActivity();
        initiateVariable();
    }

    private void takeBundleFromActivity(){
        Bundle b = getActivity().getIntent().getExtras();
        login = b.getString("name");
        lastPhoneNumber = b.getString("phone");
        uid = b.getString("uid");
        Log.d("######DEBUG ", lastPhoneNumber+" "+uid+" "+login);
    }
    private void initiateVariable(){
        messageResponse = "";
        jsonResponse=null;
        converter=null;
        successResponse=0;
        childToShow = new HashSet<>();
    }
}
