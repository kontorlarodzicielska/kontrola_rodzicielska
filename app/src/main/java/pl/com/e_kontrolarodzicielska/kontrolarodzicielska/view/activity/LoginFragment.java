package pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.R;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.utils.DatabaseMethods;

/**
 * Created by GBAR on 25.10.2016.
 */
public class LoginFragment extends AppCompatActivity implements View.OnClickListener {

    EditText etLogin;
    EditText etPassword;
    EditText etPhoneNumber;
    Button bRegister;
    Button bLogin;
    CheckBox cbRegisterAsk;
    LinearLayout llPhoneContent;

   // private View view;
    private static String LOGIN_ADDRES = "LogIn.php";
    private static String REGISTER_ADDRES = "Register.php";

    private String messageResponse;
    private JSONObject jsonResponse;
    private DatabaseMethods converter;
    private int successResponse;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initListeners();
    }

    private void initView(){
        setContentView(R.layout.activity_login);
        etLogin = (EditText) findViewById(R.id.etLogin);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        bLogin = (Button) findViewById(R.id.bLogin);
        bRegister = (Button) findViewById(R.id.bRegister);
        cbRegisterAsk = (CheckBox) findViewById(R.id.register_ok);
        llPhoneContent = (LinearLayout) findViewById(R.id.phone_content);
    }
    private void initListeners(){
        bLogin.setOnClickListener(this);
        bRegister.setOnClickListener(this);
        cbRegisterAsk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    bRegister.setEnabled(true);
                    llPhoneContent.setVisibility(View.VISIBLE);
                    bLogin.setEnabled(false);
                }
                else{
                    bRegister.setEnabled(false);
                    llPhoneContent.setVisibility(View.GONE);
                    bLogin.setEnabled(true);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == bLogin.getId()) {
            String login = etLogin.getText().toString().trim();
            String password = etPassword.getText().toString().trim();
            new LogIn().execute(login,password);
        }
        if (view.getId() == bRegister.getId()){
            String login = etLogin.getText().toString().trim();
            String password = etPassword.getText().toString().trim();
            String phone = etPhoneNumber.getText().toString().trim();
            if(login.length()>0 && password.length()>0) {
                if (phone.length() == 9) {
                    new Registration().execute(login, password, phone);
                } else {
                    Toast.makeText(getApplicationContext(), "Wrong phone number", Toast.LENGTH_SHORT).show();
                    etPhoneNumber.setBackgroundColor(Color.RED);
                }
            } else {
                Toast.makeText(getApplicationContext(), "Login and password can not be empty", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class Registration extends AsyncTask<String, String, String> {

        private String login;
        private String password;
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            messageResponse = "";
            jsonResponse = new JSONObject();
            converter = new DatabaseMethods();
            successResponse=0;
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {

            login = args[0];
            password = args[1];
            String phone = args[2];
            String uid = converter.SetUID(phone);

            // Building Parameters
            HashMap<String, String> params = new HashMap<>();
            params.put("login", login);
            params.put("password", password);
            params.put("phone", phone);
            params.put("uid",uid);

            jsonResponse = converter.PostMethodResult(params,REGISTER_ADDRES);
            try {
                messageResponse = jsonResponse.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
        /*
                }

                /**
                 * After completing background task Dismiss the progress dialog
                 * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            runOnUiThread(new Runnable() {
                public void run() {
                    // TODO: 25.03.2017 "REGISTRATION"
                    // decide what to do with registration
                    // accept : go to another activity ?
                    // reject : enter data one more time ?

                    if(successResponse==1){
                        etLogin.setText(login);
                        etPassword.setText(password);
                        cbRegisterAsk.setChecked(false);
                    }
                    //Show message on the screen
                    Toast.makeText(getApplicationContext(),messageResponse, Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    class LogIn extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            messageResponse = "";
            jsonResponse = new JSONObject();
            converter = new DatabaseMethods();
            successResponse=0;
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {

            String login = args[0];
            String password = args[1];

            // Building Parameters
            HashMap<String, String> params = new HashMap<>();
            params.put("login", login);
            params.put("password", password);

            jsonResponse = converter.PostMethodResult(params,LOGIN_ADDRES);
            try {
                successResponse = jsonResponse.getInt("success");
                messageResponse = jsonResponse.getString("message");

                if(successResponse==1){
                    String uid = jsonResponse.getString("uid");
                    String phone = jsonResponse.getString("phone");
                    Intent logInActivity = new Intent(getApplicationContext(),UserActivity.class);
                    logInActivity.putExtra("name",login);
                    logInActivity.putExtra("uid",uid);
                    logInActivity.putExtra("phone",phone);
                    startActivity(logInActivity);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            runOnUiThread(new Runnable() {
                public void run() {
                    if(successResponse!=0) {
                        etLogin.setText("");
                        etPassword.setText("");
                    }
                    //Show message on the screen
                    Toast.makeText(getApplicationContext(),messageResponse, Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
}
