package pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.model;

/**
 * Created by Użytkownik on 18.04.2017.
 */
public class Location {

    private Double lat;
    private Double lon;
    private String date;
    private String time;

    public Location(Double lat, Double lon, String date, String time) {
        this.lat = lat;
        this.lon = lon;
        this.date = date;
        this.time = time;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
