package pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.R;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.utils.DatabaseMethods;

/**
 * Created by Użytkownik on 29.11.2016.
 */
public class AddChildFragment extends BaseFragment {

    private static String ADD_CHILD_ADDRESS = "AssignChildToParent.php";
    EditText etTempPass;
    Button bAddChild;

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.add_child_fragment,container,false);
        initView();
        setListeners();
        return view;
    }
    private void initView(){
        etTempPass = (EditText) view.findViewById(R.id.etTempPass);
        bAddChild = (Button) view.findViewById(R.id.bAddChild);
    }
    private void setListeners(){
        bAddChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tempPass = etTempPass.getText().toString().trim();
                new AddChild().execute(tempPass);
            }
        });
    }


    class AddChild extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            messageResponse = "";
            jsonResponse = new JSONObject();
            converter = new DatabaseMethods();
            successResponse=0;
        }
        protected String doInBackground(String... args) {
            String tempPass = args[0];
            HashMap<String, String> params = new HashMap<>();

            params.put("uid", uid);
            params.put("tempPass", tempPass);

            jsonResponse = converter.PostMethodResult(params,ADD_CHILD_ADDRESS);
            try {
                successResponse = jsonResponse.getInt("success");
                messageResponse = jsonResponse.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            etTempPass.setText("");
            //Show message on the screen
            Toast.makeText(getContext(), messageResponse, Toast.LENGTH_SHORT).show();
        }
    }
}
