package pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.R;
import pl.com.e_kontrolarodzicielska.kontrolarodzicielska.view.utils.DatabaseMethods;

/**
 * Created by Użytkownik on 27.11.2016.
 */

public class SettignsFragment extends BaseFragment implements View.OnClickListener {

    private static String CHANGE_PHONE_ADDRES = "ChangePhoneNumber.php";
    private View view;
    TextView tvWelcomeUser;
    EditText etPhoneEdit;
    EditText etUID;
    Button bConfirmChanges;
    Spinner spRangeTimePicker;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(
                R.layout.settigns_fragment, container, false);
        bindView();
        bindListeners();
        fillData();

        return view;
    }
    private void bindView(){
        tvWelcomeUser =(TextView) view.findViewById(R.id.tvWelcomeUser) ;
        etPhoneEdit= (EditText) view.findViewById(R.id.etPhoneEdit);
        bConfirmChanges= (Button) view.findViewById(R.id.bConfirmChanges);
        etUID=(EditText) view.findViewById(R.id.etUID);
        spRangeTimePicker = (Spinner) view.findViewById(R.id.spPickTimeRange);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.range_time_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spRangeTimePicker.setAdapter(adapter);
     }
    private void bindListeners(){
        spRangeTimePicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               // Toast.makeText(getContext(),String.valueOf(i),Toast.LENGTH_LONG).show();
                rangeTime = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        bConfirmChanges.setOnClickListener(this);
    }

    private void fillData(){

        tvWelcomeUser.setText("Welcome "+login);
        etPhoneEdit.setText(lastPhoneNumber);
        etUID.setText(uid);
    }

    @Override
    public void onClick(View view) {
        String newPhoneNumber = etPhoneEdit.getText().toString().trim();
        if(!lastPhoneNumber.contains(newPhoneNumber))
            new CheckNumber().execute(uid,newPhoneNumber);
    }

    class CheckNumber extends AsyncTask<String, String, String> {

        String phone;
        String uid;
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            phone="";
            uid="";
            messageResponse = "";
            jsonResponse = null;
            converter = null;
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {

            uid = args[0];
            phone = args[1];

            // Building Parameters
            HashMap<String, String> params = new HashMap<>();
            params.put("uid", uid);
            params.put("phone", phone);

            jsonResponse = converter.PostMethodResult(params,CHANGE_PHONE_ADDRES);
            try {
                messageResponse = jsonResponse.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
        protected void onPostExecute(String file_url) {
                //Show message on the screen
            if(successResponse!=1)
                etPhoneEdit.setText(lastPhoneNumber);
            else
                lastPhoneNumber = phone;
            Toast.makeText(getActivity(),messageResponse, Toast.LENGTH_SHORT).show();
        }
    }
}
